###################
README
###################

*******************
Installation Tutorial
*******************

**This tutorial covers the installation using debian based systems. (Debian 8.0+ / Ubuntu 14.04+)**


**1 - The following packages must be properly installed/configured in your system:**

* apache2
* mysql-server
* php5-mysql
* libapache2-mod-php5
* php5-curl
* php5-mcrypt


**2 - To install, run the command:**

sudo apt-get install apache2 php5 mysql-server php5-mysql libapache2-mod-php5 php5-curl php5-mcrypt


**3 - Create a database and import flickr-image-gallery.sql(located in the project main directory) into it. The application tables will be installed.**


**4 - Inside the “flickr_settings” table, insert your Flickr API KEY in the column “vc_key” of the first row.**


**5 - Edit file application/config/database.php and use your database settings:**

        'hostname' => '',
        'username' => '',
        'password' => '',
        'database' => '',
        'dbdriver' => 'mysqli',


**6 - Edit file application/config/config.php and change the $config[‘base_url’] value to your server IP or Domain:**

$config['base_url'] = 'http://<domain>/flickr-image-gallery/';

or

$config['base_url'] = 'http://<ip>/flickr-image-gallery/';


**7 - Place the project directory in the Apache html directory**


**8 - Restart apache and mysql:**

sudo /etc/init.d/apache2 restart

sudo /etc/init.d/mysql restart


**9 - Access the project typing in your browser:**

http://<ip>/flickr-image-gallery/


*******************
Tests
*******************

**1 - There is a script available to test if the required modules are enabled, run in the browser:**

http://ip/flickr-image-gallery/test.php

The system will report if a module is not enabled, in case it happens, use the command php5enmod <module> to activate it and then restart apache2.


**2 - The Flickr API Key will be tested during the searches, if any error is encountered, it will be displayed in the web page and the user will be informed about how to proceed.**


**3 - Other errors regarding database connection and non-existent database will be displayed by CI Framework when the application is accessed.**


*******************
About
*******************

The technologies used in the project are listed below followed by the reason why they have been chosen:

* CodeIgniter Framework 3.0.6 - CodeIgniter is a powerful PHP MVC Framework. It was selected because it conceives a rapid development and deployment of the projet.

* Apache 2 - Apache is the world's most used web server. Apache configuration is simple and it does not require much time. As the project does not need a scalable and high performance server, Apache was a good choice. 

* MySQL 5.5 - MySQL is one of the world's most used relational database management system. Most of PHP Frameworks are fully compatible with MySQL, making it a good choice to this project.
