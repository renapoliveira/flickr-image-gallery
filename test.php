<?php

class Test
{

    public function check_curl()
    {
        $name = "curl";
        echo "<br/>" . $name . ((extension_loaded($name)) ? " is loaded!" : " is not loaded, please install/activate it!");
    }

    public function check_mcrypt()
    {
        $name = "mcrypt";
        echo "<br/>" . $name . ((extension_loaded($name)) ? " is loaded!" : " is not loaded, please install/activate it!");
    }

    public function check_mysql()
    {
        $name = "mysql";
        echo "<br/>" . $name . ((extension_loaded($name)) ? " is loaded!" : " is not loaded, please install/activate it!");
    }

}

$test = new Test;
$test->check_curl();
$test->check_mcrypt();
$test->check_mysql();
