<?php

class Search_model extends CI_Model
{

    private $db_name = "recent_searches";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_last_ten()
    {
        $query = $this->db->limit('10')->order_by('id', 'DESC')->get_where($this->db_name, array('id_user' => $this->session->userdata('user_id')));
        $result = $query->result();
        return $result;
    }

    public function insert($data)
    {
        if ($this->db->insert($this->db_name, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

}
