<?php

class User_model extends CI_Model
{

    private $db_name = "user";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_user($params = array())
    {
        $query = $this->db->get_where($this->db_name, $params);
        $result = $query->result();
        if (count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function insert($data)
    {
        if ($this->db->insert($this->db_name, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

}
