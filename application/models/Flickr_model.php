<?php

class Flickr_model extends CI_Model
{

    private $db_name = "flickr_settings";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_key()
    {
        $query = $this->db->get($this->db_name, array('id' => 1));
        $result = $query->result();
        if (isset($result[0])) {
            return $result[0]->vc_key;
        } else {
            show_error('Please, insert the Flickr API KEY inside the table "flickr_settings" in the column "vc_key".', 500, $heading = 'An Error Was Encountered');
            exit;
        }
    }

}
