<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"></div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger" role="alert">            
                    <?php echo validation_errors('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '); ?>
                </div>
            <?php } ?>
            <?php if (isset($_SESSION['error_msg'])) { ?>
                <div class="alert alert-danger" role="alert">            
                    <?php echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> ' .$_SESSION['error_msg']; ?>
                </div>
            <?php } ?>
            <?php echo form_open('user/register', 'method="POST" class="form-signin"'); ?>
            <h2 class="form-signin-heading">Sign up</h2>
            <?php echo form_label('Name', 'name', array('class' => 'sr-only')); ?>
            <?php echo form_input(array('name' => 'name', 'class' => 'form-control', 'placeholder' => 'Name', 'value' => set_value('name'))); ?>
            <br/>
            <?php echo form_label('Email address', 'email', array('class' => 'sr-only')); ?>
            <?php echo form_input(array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'E-mail', 'value' => set_value('email'))); ?>
            <br/>
            <?php echo form_label('Password', 'password', array('class' => 'sr-only')); ?>
            <?php echo form_password(array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'value' => '')); ?>
            <br/>
            <?php echo form_label('Confirm Password', 'password_confirm', array('class' => 'sr-only')); ?>
            <?php echo form_password(array('name' => 'password_confirm', 'class' => 'form-control', 'placeholder' => 'Password Confirmation', 'value' => '')); ?>
            <br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
            <?php echo form_close(); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"></div>
    </div>

    <br/>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <a href="<?php echo site_url('user/login'); ?>">I already have an account</a>
        </div>
    </div>

</div> 