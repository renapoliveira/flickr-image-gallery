<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"></div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger" role="alert">            
                    <?php echo validation_errors('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '); ?>
                </div>
            <?php } ?>
            <?php echo form_open('user/login', 'method="POST" class="form-signin"'); ?>
            <h2 class="form-signin-heading">Please sign in</h2>
            <?php echo form_label('Email address', 'email', array('class' => 'sr-only')); ?>
            <?php echo form_input(array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'E-mail', 'value' => set_value('email'))); ?>
            <br/>
            <?php echo form_label('Password', 'password', array('class' => 'sr-only')); ?>
            <?php echo form_password(array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'value' => '')); ?>
            <br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <?php echo form_close(); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"></div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <a href="<?php echo site_url('user/register'); ?>">I don't have an account</a>
        </div>
    </div>
</div> 