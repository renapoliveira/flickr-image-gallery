<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">    

        <title><?php echo $title; ?></title>

        <link href="<?php echo base_url('assets/css/reset.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/bootstrap-3.3.6/css/justified-nav.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/bootstrap-3.3.6/css/bootstrap.min.css') ?>" rel="stylesheet">

    </head>

    <body>
        <div class="container">

            <div class="masthead">
                <h3 class="text-muted"><?php echo $title; ?></h3> 
                <?php if (isset($logged_in)) { ?>
                    <nav>
                        <ul class="nav nav-justified">
                            <li <?php echo (isset($page) && $page == 'gallery') ? 'class="active"' : '' ?> ><a href="<?php echo site_url('gallery/show'); ?>">Search Images</a></li>
                            <li <?php echo (isset($page) && $page == 'recent_searches') ? 'class="active"' : '' ?> ><a href="<?php echo site_url('gallery/recent_searches'); ?>">Recent Searches</a></li>
                            <li ><a href="<?php echo site_url('user/logout'); ?>">Logout</a></li>
                        </ul>
                    </nav>
                    <?php } ?>
            </div>
            <br/>