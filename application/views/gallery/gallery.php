<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <?php echo form_open('gallery/show', 'method="GET"'); ?>
            <div class="input-group">
                <?php echo form_input(array('name' => 'keywords', 'class' => 'form-control', 'placeholder' => 'Search for', 'value' => (isset($keywords) && !empty($keywords) ? $keywords : ''))); ?>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Search</button>
                    <button id="reset" onclick="window.location = '<?php echo site_url('gallery/show'); ?>'" type="button" class="btn btn-default" aria-label="Help"><span class="glyphicon glyphicon glyphicon-remove"></span></button>
                </span>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-4"></div>

    </div>
</div>
<br/>
<div class="row">
    <?php
    if (isset($images) && !empty($images)) {
        foreach ($images as $photo) {
            ?>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                <?php //Adds htmlencode to title because it may use special chars and it breaks the url ?>
                <a class="thumbnail" href="<?php echo site_url('gallery/image/?' . 'id=' . $photo["id"] . '&title=' . urlencode($photo['title'])); ?>">
                    <?php echo '<img style="height:200px;" class="img-responsive" src="' . 'http://farm' . $photo["farm"] . '.static.flickr.com/' . $photo["server"] . '/' . $photo["id"] . '_' . $photo["secret"] . '.jpg" alt="' . $photo['title'] . '" title="' . $photo['title'] . '">'; ?>
                </a>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <?php if (isset($error)) { ?>
                <div class="alert alert-danger" role="alert">            
                    <?php echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> ' . $error; ?>
                </div>
            <?php } else { ?>
                No images to display
            <?php } ?>
        </div>
    <?php } ?>
</div>

<br/>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
        <nav>
            <?php
            if (isset($pagination)) {
                echo $pagination;
            }
            ?>                        
        </nav>
    </div>
</div>

<hr>

