<br/>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button class="btn btn-default" onclick="javascript:history.go(-1)">
            <span class="glyphicon glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go back
        </button>
    </div>
</div>
<br/>
<div class="row">
    <?php if (isset($biggest_image) && !empty($biggest_image)) { ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <img class="center-block" src="<?php echo $biggest_image; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" />
        </div>
    <?php } else { ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <?php if (isset($error)) { ?>
                <div class="alert alert-danger" role="alert">            
                    <?php echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> ' . $error; ?>
                </div>
                <?php } else { ?>
                No image to display
            <?php } ?>
        </div>
    <?php } ?>
</div>
<br/>

<hr>

