<br/>
<div class="row">
    <?php if (isset($searches) && !empty($searches)) { ?>
        <table class="table table-striped"> 
            <thead> 
                <tr> 
                    <th>Search</th> 
                    <th>Date</th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php foreach ($searches as $s) { ?>
                    <tr>
                        <td><a href="<?php echo site_url('gallery/show/?keywords=' . $s->vc_search); ?>"><?php echo $s->vc_search; ?></a></td> 
                        <td><?php echo $s->de_datetime; ?></td>
                    </tr> 
                <?php } ?>
            </tbody> 
        </table>
        <?php } else { ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">There is no recent search</div>
    <?php } ?>
</div>