      <footer class="footer">
        <p>&copy; <?php echo date('Y'); ?> <?php echo $title; ?>.</p>
      </footer>

    </div> 
 
    <script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap-3.3.6/js/bootstrap.min.js'); ?>"></script>
  </body>
</html>