<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Gallery
 * 
 * 
 * @package    CI
 * @subpackage Controller
 * @author     Rena Oliveira rena.p.oliveira@gmail.com
 */
class Gallery extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  get_pagination
     * 
     * @param integer $total Receives the total number of images and calculate by the per page parameter.
     * @return html
     */
    private function get_pagination($total)
    {

        $this->load->library('pagination');
        $this->pagination->config['base_url'] = site_url('gallery/show') . '/?' . (($this->input->get('keywords')) ? 'keywords=' . $this->input->get('keywords') : '');
        $this->pagination->config['total_rows'] = $total;
        $this->pagination->initialize($this->pagination->config);

        return $this->pagination->create_links();
    }

    /**
     *  search
     * 
     * @param string $query
     * @param integer $page
     * @return array
     */
    public function search($query = null, $page = 1)
    {
        //If page number is higher than one, it means the user is navigating and the search should not be saved in the db again
        if ($page == 1) {
            $this->search_model->insert(array('vc_search' => $query, 'id_user' => $this->session->userdata('user_id'), 'de_datetime' => date('Y-m-d H:i:s')));
        }
        return $this->flickr->search(urldecode($query), $page);
    }

    public function show()
    {
        if ($this->input->get('keywords') && !empty($this->input->get('keywords'))) {
            $page = ($this->input->get('page') && !empty($this->input->get('page')) ? $this->input->get('page') : 1);
            $search = $this->search($this->input->get('keywords'), $page);
            if (isset($search['stat']) && $search['stat'] == 'fail') {
                $this->data['error'] = $search['message'];
            } else {
                $this->data['images'] = $search['photos']['photo'];
                $this->data['keywords'] = $this->input->get('keywords');
                $this->data['pagination'] = $this->get_pagination($search['photos']['total']);
            }
        }

        $this->data['page'] = "gallery";
        $this->load->vars($this->data);
        $this->load_views(array('header', 'gallery/gallery', 'footer'));
    }

    public function image()
    {
        if ($this->input->get('id') && $this->input->get('title')) {
            $biggest_image = $this->flickr->get_biggest_size($this->input->get('id'));
            if (isset($biggest_image['stat']) && $biggest_image['stat'] == 'fail') {
                $this->data['error'] = $biggest_image['message'];
            } else {
                $this->data['biggest_image'] = $this->flickr->get_biggest_size($this->input->get('id'));
            }
            $this->data['image_title'] = urldecode($this->input->get('title'));
        }

        $this->load->vars($this->data);
        $this->load_views(array('header', 'gallery/image', 'footer'));
    }

    public function recent_searches()
    {
        $this->data['searches'] = $this->search_model->get_last_ten();
        $this->data['page'] = "recent_searches";
        $this->load->vars($this->data);
        $this->load_views(array('header', 'gallery/recent_searches', 'footer'));
    }

}
