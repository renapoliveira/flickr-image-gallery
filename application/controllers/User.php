<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
   * User
   * 
   * 
   * @package    CI
   * @subpackage Controller
   * @author     Rena Oliveira rena.p.oliveira@gmail.com
   */
class User extends CI_Controller
{

    private $data;
    private $sess_logged_in = 'logged_in';
    private $sess_user_name = 'user_name';
    private $sess_user_email = 'user_email';
    private $sess_user_id = 'user_id';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->model('user_model');
        $this->data = array(
            'title' => "Flickr Image Gallery",
        );
    }
    
    /**
     * set_session
     * 
     * @param string $name
     * @param string $email
     * @param integer $id
     */

    private function set_session($name, $email, $id)
    {
        $session = array(
            $this->sess_logged_in => true,
            $this->sess_user_name => $name,
            $this->sess_user_name => $email,
            $this->sess_user_id => $id
        );
        $this->session->set_userdata($session);
    }

    
    public function register()
    {
        if ($this->session->userdata('logged_in')) {
            redirect();
        }
        $this->form_validation->set_rules('name', 'Name', 'trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__email_already_registered');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required');

        if ($this->form_validation->run()) {
            $id = $this->user_model->insert(array(
                'vc_name' => $this->input->post('name'),
                'vc_email' => $this->input->post('email'),
                'vc_password' => $this->encrypt->encode($this->input->post('password')),
            ));
            if ($id) {
                $this->set_session($this->input->post('name'), $this->input->post('email'), $id);
                redirect();
            } else {
                $this->session->set_flashdata('error_msg','Error inserting the user to the database');
                redirect(site_url('user/register'));
            }
        } else {
            $this->load->vars($this->data);
            $this->load->view('header');
            $this->load->view('user/register');
            $this->load->view('footer');
        }
    }

    public function login()
    {
        if ($this->session->userdata('logged_in')) {
            redirect();
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__validate_user');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run()) {
            $user = $this->user_model->get_user(array('vc_email' => $this->input->post('email')));
            $this->set_session($user->vc_name, $user->vc_email, $user->id);
            redirect();
        } else {
            $this->load->vars($this->data);
            $this->load->view('header');
            $this->load->view('user/login');
            $this->load->view('footer');
        }
    }

    public function _email_already_registered()
    {
        $user = $this->user_model->get_user(array('vc_email' => $this->input->post('email')));
        if ($user) {
            $this->form_validation->set_message('_email_already_registered', 'E-mail already registered');
            return false;
        }
        return true;
    }

    public function _validate_user()
    {
        $user = $this->user_model->get_user(array('vc_email' => $this->input->post('email')));
        if ($user && ($this->encrypt->decode($user->vc_password) == $this->input->post('password'))) {
            return true;
        } else {
            $this->form_validation->set_message('_validate_user', 'E-mail or Password invalid');
            return false;
        }
    }

    public function logout()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect();
        }
        $unset = array($this->sess_logged_in, $this->sess_user_name, $this->sess_user_email, $this->sess_user_id);
        foreach ($unset as $u) {
            $this->session->unset_userdata($u);
        }
        redirect();
    }

}
