<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Main
 * 
 * 
 * @package    CI
 * @subpackage Controller
 * @author     Rena Oliveira rena.p.oliveira@gmail.com
 */
class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')) {
            redirect(site_url('gallery/show'));
        } else {
            redirect(site_url('user/login'));
        }
    }

}
