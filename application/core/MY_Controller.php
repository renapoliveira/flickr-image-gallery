<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MY_Controller
 * 
 * -This core Controller is used for pages that require the user to be logged in.
 * -It loads models, custom libraries, data and provides shared methods 
 * 
 * @package    CI
 * @subpackage Controller
 * @author     Rena Oliveira rena.p.oliveira@gmail.com
 */
class MY_Controller extends CI_Controller
{

    protected $data;

    public function __construct()
    {
        parent::__construct();
        $this->load_models();
        $this->load_libraries();
        $this->check_session();
        $this->set_data();
    }

    private function load_models()
    {
        $this->load->model('flickr_model');
        $this->load->model('search_model');
    }

    private function load_libraries()
    {
        $this->load->library('flickr', array('key' => $this->flickr_model->get_key()));
    }

    private function check_session()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect();
        }
    }

    private function set_data()
    {
        $this->data = array(
            'title' => "Flickr Image Gallery",
            'logged_in' => true
        );
    }

    /**
     * load_views
     * 
     * @param array $views
     */
    protected function load_views($views)
    {
        foreach ($views as $v) {
            $this->load->view($v);
        }
    }

}
