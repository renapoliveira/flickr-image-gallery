<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Flickr
{

    private $key;

    public function __construct($params)
    {
        $this->key = $params['key'];
    }

    /**
     * 
     * @param string $url
     * @return array
     * 
     */
    private function curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($retcode == 200) {
            return $data;
        } else {
            return null;
        }
    }

    /**
     * search
     * 
     * @param string $query
     * @param integer $page
     * @param string $format
     * @return array
     * 
     */
    public function search($query = null, $page = 1, $format = 'php_serial')
    {
        $args = array(
            'method' => 'flickr.photos.search',
            'api_key' => $this->key,
            'text' => urlencode($query),
            'user_id' => null,
            'per_page' => 5,
            'format' => $format,
            'page' => $page
        );
        $url = 'http://flickr.com/services/rest/?';
        $search = $url . http_build_query($args);
        $result = $this->curl($search);
        if ($format == 'php_serial') {
            $result = unserialize($result);
        }
        if (isset($result) && isset($result['stat']) && $result['stat'] == 'fail') {
            if ($result['code'] == 100) {
                show_error('Your Flickr API Key is invalid, please update it inside the "flickr_settings" table.', 500, $heading = 'An Error Was Encountered');
            }
            exit;
        }
        return $result;
    }

    /**
     * get_sizes
     * 
     * @param integer $photo_id
     * @param string $format
     * @return array
     */
    private function get_sizes($photo_id, $format = 'php_serial')
    {
        $args = array(
            'method' => 'flickr.photos.getSizes',
            'api_key' => $this->key,
            'format' => $format,
            'photo_id' => $photo_id
        );
        $url = 'http://flickr.com/services/rest/?';
        $search = $url . http_build_query($args);
        $result = $this->curl($search);
        if ($format == 'php_serial') {
            $result = unserialize($result);
        }
        if (isset($result) && isset($result['stat']) && $result['stat'] == 'fail') {
            if ($result['code'] == 100) {
                show_error('Your Flickr API Key is invalid, please update it inside the "flickr_settings" table.', 500, $heading = 'An Error Was Encountered');
                exit;
            }
        }
        return $result;
    }

    /**
     * get_biggest_size
     * 
     *  Some images in Flickr do not have Original or Large size available, so a workaround is needed.

     * @param integer $photo_id
     * @return string
     */
    public function get_biggest_size($photo_id)
    {
        $sizes = $this->get_sizes($photo_id);
        if (isset($sizes) && !empty($sizes) && $sizes['stat'] == 'ok') {
            foreach ($sizes['sizes']['size'] as $size) {
                if ($size['label'] == 'Original') {
                    return $size['source'];
                }
            }
            foreach ($sizes['sizes']['size'] as $size) {
                if ($size['label'] == 'Large') {
                    return $size['source'];
                }
            }
            foreach ($sizes['sizes']['size'] as $size) {
                if ($size['label'] == 'Medium') {
                    return $size['source'];
                }
            }
            foreach ($sizes['sizes']['size'] as $size) {
                if ($size['label'] == 'Small') {
                    return $size['source'];
                }
            }
        } else {
            return $sizes;
        }
    }

}
