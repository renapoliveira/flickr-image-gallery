<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Pagination extends CI_Pagination
{

    public $config = array();

    public function __construct()
    {
        parent::__construct();
        $this->set_defaults();
    }

    private function set_defaults()
    {
        $this->config['full_tag_open'] = "<ul class='pagination pagination-md'>";
        $this->config['full_tag_close'] = "</ul>";
        $this->config['num_tag_open'] = '<li>';
        $this->config['num_tag_close'] = '</li>';
        $this->config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $this->config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $this->config['next_tag_open'] = "<li>";
        $this->config['next_tagl_close'] = "</li>";
        $this->config['prev_tag_open'] = "<li>";
        $this->config['prev_tagl_close'] = "</li>";
        $this->config['first_tag_open'] = "<li>";
        $this->config['first_tagl_close'] = "</li>";
        $this->config['last_tag_open'] = "<li>";
        $this->config['last_tagl_close'] = "</li>";
        $this->config['first_link'] = 'First';
        $this->config['last_link'] = 'Last';

        $this->config['use_page_numbers'] = TRUE;
        $this->config['page_query_string'] = TRUE;
        $this->config['query_string_segment'] = 'page';
        $this->config['per_page'] = 5;
        //num_links = number of links in each side of the selected page
        $this->config['num_links'] = 2;
    }

}
