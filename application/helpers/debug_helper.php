<?php

/**
 * This Helper has debugging purposes
 */
if (!defined('BASEPATH'))
    exit('Sem acesso');

/**
 * 
 * @param type $data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}
